package com.example.www.tel100reaksjonsspill;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class ReaksjonsSpill extends AppCompatActivity {
    boolean kanTrykke = false;
    boolean timerKjorer = false;
    boolean konkuranseTid = false;
    int player1_score = 0;
    int player2_score = 0;

    static Timer minTimer = new Timer();
    RelativeLayout layout;

    class MinTimer extends TimerTask {
        @Override
        public void run(){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    layout.setBackgroundColor(Color.GREEN);
                    kanTrykke = true;
                    konkuranseTid = true;
                }
            });

            timerKjorer = false;
        }
    }

    class DelayTimer extends TimerTask {
        @Override
        public void run(){
            konkuranseTid = false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reaksjons_spill);
        layout = (RelativeLayout) findViewById(R.id.relative_layout_bakgrunn);

        final Button spiller1 = (Button) findViewById(R.id.spiller1);
        final Button spiller2 = (Button) findViewById(R.id.spiller2);
        final TextView txt_spiller1 = (TextView) findViewById(R.id.spiller1_score);
        final TextView txt_spiller2 = (TextView) findViewById(R.id.spiller2_score);

        final Intent intentVictory = new Intent(ReaksjonsSpill.this, ActivityVictory.class);

        spiller1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(kanTrykke){
                    player1_score++;
                    if(player1_score >= 2){
                        intentVictory.putExtra("Winner", '1');
                        startActivity(intentVictory);
                    }
                    minTimer.schedule(new DelayTimer(), 500);
                }else if (konkuranseTid){

                } else{
                    player1_score = Math.max(--player1_score, 0);
                    /*
                    player1_score--;
                    if(player1_score < 0){
                        player1_score = 0;
                    }
                    */
                }
                txt_spiller1.setText(Integer.toString(player1_score));
                nyRunde();
            }
        });
        spiller2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(kanTrykke){
                    player2_score++;
                    if(player2_score >= 2){
                        intentVictory.putExtra("Winner", '2');
                        startActivity(intentVictory);
                    }
                    minTimer.schedule(new DelayTimer(), 500);
                }else if (konkuranseTid){

                }else{
                    player2_score = Math.max(--player2_score, 0);
                    /*
                    player2_score--;
                    if(player2_score < 0){
                        player2_score = 0;
                    }
                    */
                }
                txt_spiller2.setText(Integer.toString(player2_score));
                nyRunde();
            }
        });
        nyRunde();
    }

    void nyRunde(){
        layout.setBackgroundColor(Color.BLUE);
        kanTrykke = false;
        int delay = (3 + new Random().nextInt(7)) * 1000;
        if(!timerKjorer){
            timerKjorer = true;
            minTimer.schedule(new MinTimer(), delay);
        }
    }
}
