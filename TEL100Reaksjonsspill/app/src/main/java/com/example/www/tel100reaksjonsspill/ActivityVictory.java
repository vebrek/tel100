package com.example.www.tel100reaksjonsspill;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ActivityVictory extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_victory);
        TextView textWinner = (TextView) findViewById(R.id.txt_winner);
        Bundle bundle = getIntent().getExtras();

        textWinner.setText("Winner is Player: " + bundle.get("Winner"));
        textWinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityVictory.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
