package com.example.www.tel100reaksjonsspill;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button star_spill_knapp = (Button) findViewById(R.id.start_spill);
        star_spill_knapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_start_spill = new Intent(MainActivity.this, ReaksjonsSpill.class);
                startActivity(intent_start_spill);
            }
        });
    }
}
